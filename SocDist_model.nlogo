globals                                                       ;global variables
[
  t                                                           ;t = Time
  loop_counter                                                ;loop_counter counts the loops needed for the sensor. All sensors get updated simultaneously
]

e-pucks-own                                                   ;e-puck variables
[
  prob_stop                                                   ;stopping probability that SpontStop and SocDist calculate
  standing_time                                               ;how long e-pucks still have to stand around before they move again
  standing                                                    ;standing is binary: 0 = e-puck is moving, 1 = e-puck is standing
  sensor                                                      ;e-pucks have 8 sensors. The sensor variable shows which sensor has detected an obstacle
]

breed [wall_turtles wall_turtle]                              ;walls consist out of other agents. They stand still, but can be detected by e-pucks
breed [e-pucks e-puck]                                        ;moving agents represent e-pucks
patches-own [quality]                                         ;patches have a certain quality i.e. the amount of light that is there (illuminance level)

to setup
  clear-all                                                   ;this clears everything
  reset-ticks                                                 ;resets the ticks (basically a time step counter)
  CREATE_WALLS_AND_GRADIENT                                   ;the arena gets made: walls are put in place and a light gradient is generated
  CREATE_OBSTACLE                                             ;Create the obstacle in the middle
  ROBOT_STARTING_DISTRIBUTION                                 ;e-pucks are not placed randomly, but within a certain starting area
  SETUP_ROBOTS                                                ;e-pucks get initialized, they get a certain size to be visible, randomly oriented and so on
end

to go                                                         ;after pressing "go", the experiment starts
  ask e-pucks  [move]                                         ;e-pucks move according to the procedure (see below)
  set t t + 1                                                 ;time goes by. Each time step, time increases by one time unit
  tick                                                        ;a tick happens
end


to move                                                       ;this shows how the e-pucks move
  ifelse standing_time >= 0                                   ;if the standing time is greater or equal to 0
      [set standing 1 set color red]                              ;...e-pucks stand around and are red
      [set standing 0 set color green]                            ;...otherwise, they don't stand around and move

  ifelse standing = 1                                         ;if e-pucks are standing around
  [
    set standing_time standing_time - 1                           ;...they reduce their standing time by one time unit each time step
  ]
  [
    let check 0                                                                   ;e-pucks check if other e-pucks are in their perception radius
    if count other e-pucks in-radius (radius + 3.5) > 0 [set check 1]             ;if other e-pucks are in their perception radius (+3.5, because it is measured from the center of the agent and it has a radius of 3.5), then check gets set to 1

    if METHOD = "SOCDIST"                                                         ;SocDist - the main algorithm in the study "Social Distancing in Robot Swarms: Modulating Exploitation and Exploration without Signal Exchange"
      [
        set prob_stop ((quality * influence_gradient) - (check * 0.5))            ;e-pucks running SocDist set their stopping probability (prob_stop) equal to the environment quality multiplied by a constant (influence_gradient) and then reduce it, if another e-puck is around.
        avoid-walls-and-e-pucks                                                   ;all e-pucks avoid collissions
        move_or_not                                                               ;and decide wheter they move or not
      ]

    if METHOD = "SPONTSTOP"                                                       ;SpontStop is SocDist without the asocial component
      [
        set prob_stop (quality * influence_gradient)
        avoid-walls-and-e-pucks
        move_or_not
      ]

    if METHOD = "BEECLUST"                                                        ;BEECLUST is a bio-inspired algorithm
      [
        avoid_walls                                                               ;it avoids walls. Other ebugs are not directly avoided, but there are still no collissions. See procedure "BEECLUST" for more than that.
        BEECLUST                                                                  ;refers to the procedure "BEECLUST" (see below)
      ]

    if METHOD = "UPHILL"                                                          ;UPHILL agents are "greedy" they check their surroundings for a better location and move towards it
      [
        UPHILLER                                                                  ;refers to the procedure "UPHILL" (see below)
        avoid-walls-and-e-pucks
      ]

    if METHOD = "RANDOMWALK"                                                      ;Randomwalkers walk randomly
      [
        avoid-walls-and-e-pucks
        RANDOMWALK                                                                ;refers to the procedure "UPHILL" (see below)
      ]
  ]
end


to move_or_not
  let z random-float 1                                                            ;Random number between 0 and 1 is generated
  ifelse z < prob_stop                                                            ;if the number is lower than prob_stop
      [set standing_time standing_time + countdown] ;IF                           ;...there is no movement
      [fd speed]                                    ;ELSE                         ;...otherwise, the e-pucks move
end

to sensing
  set sensor [100 0]                                                              ;standard value for the sensor
  set loop_counter 0                                                              ;no loops happened so far
  rt 19                                                                           ;rt = right turn. The e-puck turns right 19 degreas
  sensor-loop                                                                     ;refers to the procedure "sensor-loop" (see below)
  repeat 7                                                                        ;this gets repeated 7 times because e-puck has 8 sensors and one was done already
  [
    rt 46                                                                         ;turns exactly this much to stay in the same orientation then it was before "sensing"
    sensor-loop
  ]
end

to-report lfw                                                                                                   ;look for walls
  let value 0                                                                                                   ;standard value is 0 (no wall)
  carefully [set value distance one-of other wall_turtles in-cone radius 45 with-min [distance myself]][]       ;distance to the next wall gets estimated
  report value                                                                                                  ;value gets reported
end

to sensor-loop                                                                                                                  ;here, every sensor check if it is the closest one to the wall
  let temp lfw                                                                                                                  ;temp registers lfw
  if temp > 0 and temp < item 0 sensor [set sensor replace-item 0 sensor temp set sensor replace-item 1 sensor loop_counter]    ;every sensor checks if it is the closest one to the wall. this is done with a list. if one is closer than the others, it gets put in the list and the other gets removed.
  set loop_counter loop_counter + 1                                                                                             ;for every checked sensor, the loop_counter goes up by one million just kidding by one of course
end

to BEECLUST
  ifelse count other e-pucks in-cone (radius) 180 > 0                             ;BEECLUST checks for other e-pucks in the perception radius
  [
    ifelse  random 2 = 1                                                              ;...if there is one it either
    [
      set heading heading - (90 + random 90)                                          ;turns to the right
    ]
    [
      set heading heading + (90 + random 90)                                          ;or the left with a 50/50 chance
    ]
    set standing_time ((quality / 160) * 480)                                         ;the standing time gets adjusted according to the quality
  ]
  [
   fd speed                                                                       ;if there is no other e-puck, they move straight ahead
  ]
end

to UPHILLER
  set heading towards one-of neighbors4 with-max [quality]                        ;uphiller checks the neighboring patches for the one with the best quality and turns towards it
  if count wall_turtles in-cone 8 45 = 0                                          ;if there is no wall in sight...
  [
    fd speed                                                                          ;...it moves forward
  ]
end

to RANDOMWALK
  fd speed                                                                        ;moves forward
  set heading (heading - 45 + (random 90))                                        ;adjusts its heading randomly
end

to avoid-walls-and-e-pucks                                                       ;avoiding walls and e-pucks has a subprocedure where only walls get avoided...
  avoid_walls                                                                    ;...this is it. See below for more details
  if count e-pucks in-cone (radius) 90 > 1                                       ;If other epucks are in the perception radius
  [
  ifelse  random 2 = 1                                                           ;e-pucks turn away either to the left or the right
    [
      set heading (heading - (75))
    ]
    [
      set heading (heading + (75))
    ]
  ]

end

to avoid_walls
  if count wall_turtles in-radius 3.5 > 0                                        ;if there is a wall in the perception radius

  [
    sensing
    if item 0 sensor < 3.5                                                       ;we check which sensor is the closest to it
    [
      let old_h heading
      set heading                                                                ;depending on that, collission avoidance picks a more or less strict turning angle
      (
        ifelse-value
        item 1 sensor = 0 [heading + (19 - 180)]
        item 1 sensor = 1 [heading + (65 - 180)]
        item 1 sensor = 2 [heading + (111 - 180)]
        item 1 sensor = 3 [heading + (157 - 180)]
        item 1 sensor = 4 [heading + (203 - 180)]
        item 1 sensor = 5 [heading + (249 - 180)]
        item 1 sensor = 6 [heading + (295 - 180)]
        item 1 sensor = 7 [heading + (341 - 180)]
    )
      fd 3.5 - item 0 sensor
      set heading old_h
    ]

      set heading
      (
        ifelse-value
        item 1 sensor = 0 [heading - 70 + random-float 5 - random-float 5]
        item 1 sensor = 1 [heading - 50 + random-float 5 - random-float 5]
        item 1 sensor = 2 [heading - 30 + random-float 5 - random-float 5]
        item 1 sensor = 3 [heading - 20 + random-float 5 - random-float 5]
        item 1 sensor = 4 [heading + 20 + random-float 5 - random-float 5]
        item 1 sensor = 5 [heading + 30 + random-float 5 - random-float 5]
        item 1 sensor = 6 [heading + 50 + random-float 5 - random-float 5]
        item 1 sensor = 7 [heading + 70 + random-float 5 - random-float 5]
    )
  ]
end

;SETUP PROCEDURES

to CREATE_WALLS_AND_GRADIENT
  ask patches
  [
    if pxcor = max-pxcor or pxcor = min-pxcor                                ;the patches on the left and the right...
        [sprout-wall_turtles 1]                                              ;...produce a piece of wall
    if pycor = max-pycor or pycor = min-pycor                                ;the patches on top and on bottom...
        [sprout-wall_turtles 1]                                              ;...produce a piece of walll
    set quality pxcor                                                        ;quality of patches is directly dependant on the x-coordinate
    set pcolor scale-color green quality 0 161                               ;this is shown to the user by color
  ]
end

to CREATE_OBSTACLE                                                           ;Obstacle gets created by asking the right patches to produce a piece of wall
  ;BUILDING BLOCK A
  ask patches
  [if pxcor = int (max-pxcor * 0.6)
  [
  if pycor > int (max-pycor * 0.3) and pycor < int (max-pycor * 0.7)
  [sprout-wall_turtles 1]
  ]
  ]

  ;BUILDING BLOCK B1
  let x2cr int (max-pxcor * 0.6)
  let y2cr int (max-pycor * 0.7)
  while [x2cr > int (max-pxcor * 0.5375)]
  [
  ask patch x2cr y2cr [sprout-wall_turtles 1]
  set x2cr x2cr - 1
  set y2cr y2cr + 1
  ]

  ;BUILDING BLOCK B2
  let xcr int (max-pxcor * 0.6)
  let ycr int (max-pycor * 0.3)
  while [xcr > int (max-pxcor * 0.5375)]
  [
  ask patch xcr ycr [sprout-wall_turtles 1]
  set xcr xcr - 1
  set ycr ycr - 1
  ]

  ;BUILDING BLOCK C1
  let x3cr int (max-pxcor * 0.5375)
  let y3cr int (max-pycor * 0.79)
  while [x3cr > int (max-pxcor * 0.49375)]
  [
  ask patch x3cr y3cr [sprout-wall_turtles 1]
  set x3cr x3cr - 1
  ]

  ;BUILDING BLOCK C2
  let x4cr int (max-pxcor * 0.5375)
  let y4cr int (max-pycor * 0.2)
  while [x4cr > int (max-pxcor * 0.49375)]
  [
  ask patch x4cr y4cr [sprout-wall_turtles 1]
  set x4cr x4cr - 1
  ]
end

to ROBOT_STARTING_DISTRIBUTION                                 ;robots start in a confined starting area
  create-e-pucks nr_turtles [                                  ;the number chosen in "nr_turtles" (slider in the interface) of e-pucks gets created
      set xcor (random  24) + 12                               ;x-coordinate gets chosen for each one, quite randomly
      set ycor (random 35) + 35                                ;y-coordinate gets chosen for each one, quite randomly

      while [count e-pucks in-radius 8.5 > 1]                  ;while e pucks are to close together, they get a new starting position
      [
        let z random-float 1
        ifelse z > 0.5
        [set xcor xcor + 1]
        [set ycor (random 35) + 35]
      ]
    ]
end

to SETUP_ROBOTS                                               ;e-puck setup includes resetting the sensor and aesthetic changes...
  ask e-pucks
  [
    set sensor -1
    set standing_time 0
    set color blue
    set shape "circle"
    set size 7
    set heading random-float 360                              ;...as well as randomly orientating them
  ]

  ask wall_turtles
  [
    set color yellow set size 2 set shape "square"           ;wall_turtles get an appropriate shape and size
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
206
10
862
447
-1
-1
4.0
1
10
1
1
1
0
0
0
1
0
161
0
106
1
1
1
ticks
30.0

BUTTON
9
18
72
51
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
10
108
182
141
nr_turtles
nr_turtles
0
100
15.0
1
1
NIL
HORIZONTAL

SLIDER
10
143
182
176
speed
speed
0
10
1.0
1
1
NIL
HORIZONTAL

BUTTON
80
18
143
51
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
10
213
182
246
influence_gradient
influence_gradient
0
0.02
0.0065
0.0001
1
NIL
HORIZONTAL

SLIDER
10
178
182
211
radius
radius
0
50
14.0
1
1
NIL
HORIZONTAL

SLIDER
10
248
182
281
countdown
countdown
0
30
24.0
1
1
NIL
HORIZONTAL

CHOOSER
10
62
180
107
METHOD
METHOD
"SPONTSTOP" "BEECLUST" "UPHILL" "SOCDIST" "RANDOMWALK"
3

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment_IT" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="5"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment_SHORT TIME" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="7500"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="5"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment_SUPER SHORT TIME" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="5000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="5"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment_RADIUS" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment_RADIUS_IT_045" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment_NORMAL TIME" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="7500"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="5"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NORMAL_TIME_SPECIAL CONDITIONS" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.17"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="OPTIMUM_SEARCH" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="15000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.17"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="OPTIMUM_SEARCH_10k" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.17"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="OPTIMUM_SEARCH_6k" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="6000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.17"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="OPTIMUM_SEARCH_4k" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.17"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="OPTIMUM_SEARCH_3k" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3000"/>
    <metric>count ebugs with [xcor &gt; 50]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.17"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_turtles">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NEW_MAP_OPTIMUM_SEARCH_10K" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 80]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.006"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="65"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NEW_MAP_swarmsize_test_8k" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="8000"/>
    <metric>count ebugs with [xcor &gt; 80] / (count ebugs)</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.006"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
      <value value="30"/>
      <value value="50"/>
      <value value="100"/>
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NEW_MAP_IG_test" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="8000"/>
    <metric>count ebugs with [xcor &gt; 80]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.005"/>
      <value value="0.0055"/>
      <value value="0.006"/>
      <value value="0.0065"/>
      <value value="0.007"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NEW_MAP_IE_test" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="8000"/>
    <metric>count ebugs with [xcor &gt; 80]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.006"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.05"/>
      <value value="0.1"/>
      <value value="0.2"/>
      <value value="0.5"/>
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="REALMAP_OPTIMUM_SEARCH_10K" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="A_VS_B_r20" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="A_VS_B_r15" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A&quot;"/>
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="A_VS_B_C_D_radius only_10k" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;D&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="6000"/>
    <metric>count turtles with [pxcor &gt; (max-pxcor * ( 4 / 5 ))]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="a1a2etc" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; (max-pxcor * (1 / 5))]</metric>
    <metric>count ebugs with [xcor &gt; (max-pxcor * (2 / 5))]</metric>
    <metric>count ebugs with [xcor &gt; (max-pxcor * (3 / 5))]</metric>
    <metric>count ebugs with [xcor &gt; (max-pxcor * (4 / 5))]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A1&quot;"/>
      <value value="&quot;A2&quot;"/>
      <value value="&quot;B&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; (max-pxcor * (4 / 5))]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A1&quot;"/>
      <value value="&quot;A2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
      <value value="3"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="30"/>
      <value value="60"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="thebugone" repetitions="1000" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; (max-pxcor * (4 / 5))]</metric>
    <metric>count ebugs with [xcor &lt; (max-pxcor - (max-pxcor * (4 / 5)))]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A1&quot;"/>
      <value value="&quot;A2&quot;"/>
      <value value="&quot;B&quot;"/>
      <value value="&quot;C&quot;"/>
      <value value="&quot;D&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Random_Distri_Test_10min600steps" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A1&quot;"/>
      <value value="&quot;A2&quot;"/>
      <value value="&quot;B&quot;"/>
      <value value="&quot;C&quot;"/>
      <value value="&quot;D&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="MAZE_TEST" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <metric>count ebugs with [xcor &gt; 112]</metric>
    <metric>count ebugs with [xcor &gt; 96]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A1&quot;"/>
      <value value="&quot;A2&quot;"/>
      <value value="&quot;B&quot;"/>
      <value value="&quot;C&quot;"/>
      <value value="&quot;D&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="A3 vs everyone" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <metric>count ebugs with [xcor &gt; 112]</metric>
    <metric>count ebugs with [xcor &gt; 96]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A1&quot;"/>
      <value value="&quot;A2&quot;"/>
      <value value="&quot;A3&quot;"/>
      <value value="&quot;B&quot;"/>
      <value value="&quot;C&quot;"/>
      <value value="&quot;D&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.004"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;WALL&quot;"/>
      <value value="&quot;MAZE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="simplewuse" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;A1&quot;"/>
      <value value="&quot;A2&quot;"/>
      <value value="&quot;B&quot;"/>
      <value value="&quot;C&quot;"/>
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.006"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="simple_optimization" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.006"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="simpleVSbeeclust_02_countdown" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.006"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
      <value value="5"/>
      <value value="7"/>
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="simpleVSbeeclust_CD3_simpi" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.006"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <steppedValueSet variable="simp_i" first="0.1" step="0.1" last="1"/>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="ebug_simple" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="ebug_simple_simpi" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.1"/>
      <value value="0.2"/>
      <value value="0.3"/>
      <value value="0.4"/>
      <value value="0.5"/>
      <value value="0.6"/>
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="simpleVSBee" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 128]</metric>
    <metric>count ebugs with [xcor &gt; 120]</metric>
    <metric>count ebugs with [xcor &gt; 114]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SimpleVSBee_realwall" repetitions="500" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 160 * 0.8]</metric>
    <metric>count ebugs with [xcor &gt; 160 * 0.75]</metric>
    <metric>count ebugs with [xcor &gt; 160 * 0.66666667]</metric>
    <metric>count ebugs with [xcor &gt; 160 * 0.6]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SimpleVSBee_maze" repetitions="500" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 160 * 0.8]</metric>
    <metric>count ebugs with [xcor &gt; 160 * 0.75]</metric>
    <metric>count ebugs with [xcor &gt; 160 * 0.66666667]</metric>
    <metric>count ebugs with [xcor &gt; 160 * 0.6]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="bottleneck_test" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 160 * 0.8]</metric>
    <metric>count ebugs with [xcor &gt; 160 * 0.75]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="no_obstacle" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; 160 * 0.8]</metric>
    <metric>count ebugs with [xcor &gt; 160 * 0.75]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="local_optimum_first_test" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; (160 * 0.8)]</metric>
    <metric>count ebugs with [xcor &gt; (160 * 0.75)]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="local_optimum_first_test" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; (160 * 0.8)]</metric>
    <metric>count ebugs with [xcor &gt; (160 * 0.75)]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="local_optimum_2" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; (160 * 0.8)]</metric>
    <metric>count ebugs with [xcor &gt; (160 * 0.75)]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="local_optimum_bigTRAP" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; (160 * 0.8)]</metric>
    <metric>count ebugs with [xcor &gt; (160 * 0.75)]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="C_D_SIMPLE_MAZE" repetitions="1000" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="600"/>
    <metric>count ebugs with [xcor &gt; (160 * 0.8)]</metric>
    <metric>count ebugs with [xcor &gt; (160 * 0.75)]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;C&quot;"/>
      <value value="&quot;D&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Mean_XCOR" repetitions="200" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;B&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="bigsize_meanXCOR" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;B&quot;"/>
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="20"/>
      <value value="30"/>
      <value value="40"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Wusel_SwarmSize" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>sum [xcor] of ebugs</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="3"/>
      <value value="5"/>
      <value value="7"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Beeclust_SwarmSize" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>sum [xcor] of ebugs</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;C&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="3"/>
      <value value="5"/>
      <value value="7"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NS_SwarmSize" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>sum [xcor] of ebugs</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="3"/>
      <value value="5"/>
      <value value="7"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Wusel_SwarmSize2" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>sum [xcor] of ebugs</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="3"/>
      <value value="5"/>
      <value value="7"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.00625"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Wusel_SwarmSize3" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>mean [xcor] of ebugs</metric>
    <metric>sum [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Ohne_SwarmSize3" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>mean [xcor] of ebugs</metric>
    <metric>sum [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Wusel_BigSize3" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>mean [xcor] of ebugs</metric>
    <metric>sum [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="55"/>
      <value value="60"/>
      <value value="65"/>
      <value value="70"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Ohne_BigSize3" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>mean [xcor] of ebugs</metric>
    <metric>sum [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="55"/>
      <value value="60"/>
      <value value="65"/>
      <value value="70"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment" repetitions="20" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1200"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SINGLE_SIMPEL_BEE" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="18"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SINGLE_VS_SIMPLE_VS_BEE_15" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SINGLE_VS_SIMPLE_VS_BEE_10" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SINGLE_VS_SIMPLE_VS_BEE_5" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Bee_Swarmsize_check" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Simple_Swarmsize_check_lownumbers" repetitions="20" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Single_Swarmsize_check" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SINGLE_VS_SIMPLE_VS_BEE_15_2" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_15" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_10" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_5" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Single_Swarmsize_check_lownumbers" repetitions="20" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Bee_Swarmsize_check_lownumbers" repetitions="20" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="ALLFIVE_Swarmsize_check_LARGE" repetitions="1000" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Simple VS Single Low Numbers" repetitions="1000" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
      <value value="4"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="obstacle" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>count ebugs with [xcor &gt; 70 and xcor &lt; 100]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="obstacle_simpl" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>count ebugs with [xcor &gt; 70 and xcor &lt; 100]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Bottle5" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;BOTTLENECK_A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Bottle10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;BOTTLENECK_A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Bottle15" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;BOTTLENECK_A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="obstacle_bee" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>count ebugs with [xcor &gt; 70 and xcor &lt; 100]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LO?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Single_Swarmsize_check_highnumbers" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
      <value value="55"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Simple_Swarmsize_check_highnumbers" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
      <value value="55"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Bee_Swarmsize_check_highnumbers" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
      <value value="20"/>
      <value value="25"/>
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
      <value value="55"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4801"/>
    <metric>sum [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_15_APRIL" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_10_APRIL" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_5_APRIL" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_15_APRIL_100runs" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_10_APRIL_100runs" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="AllFIVE_5_APRIL_100runs" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Single_Swarmsize_check_detail_numbers" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Simple_Swarmsize_check_detailnumbers" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Bee_Swarmsize_check_detailnumbers" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Random_Swarmsize_check_detailnumbers" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Single_VS_Single_ONE_check_detail_numbers" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Bee_Swarmsize_20percentshorter" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3840"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Simple_Swarmsize_20percentshorter" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3840"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="obstacle_simpl" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>count ebugs with [(xcor &gt; 70 and xcor &lt; 100) and (ycor &gt; 20 and ycor &lt; 105)]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="obstacle_singl" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>count ebugs with [(xcor &gt; 70 and xcor &lt; 100) and (ycor &gt; 20 and ycor &lt; 105)]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="obstacle_bee" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>count ebugs with [(xcor &gt; 70 and xcor &lt; 100) and (ycor &gt; 20 and ycor &lt; 105)]</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Swarmsize_Simple_NEW" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>time1</metric>
    <metric>time2</metric>
    <metric>time3</metric>
    <metric>time4</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SIMPLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Swarmsize_Single_NEW" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>time1</metric>
    <metric>time2</metric>
    <metric>time3</metric>
    <metric>time4</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SINGLEWUSEL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Swarmsize_BEE_NEW" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>time1</metric>
    <metric>time2</metric>
    <metric>time3</metric>
    <metric>time4</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Swarmsize_RANDOM_NEW" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>time1</metric>
    <metric>time2</metric>
    <metric>time3</metric>
    <metric>time4</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SOCDIST2" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>time1</metric>
    <metric>time2</metric>
    <metric>time3</metric>
    <metric>time4</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_7" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>time1</metric>
    <metric>time2</metric>
    <metric>time3</metric>
    <metric>time4</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;JUSTASSI&quot;"/>
      <value value="&quot;JUSTASSI2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_7_global+localround" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4800"/>
    <metric>time1</metric>
    <metric>time2</metric>
    <metric>time3</metric>
    <metric>time4</metric>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;JUSTASSI&quot;"/>
      <value value="&quot;JUSTASSI2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Test_timecheck" repetitions="3" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4801"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Test_timecheck" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4801"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;REVERSE&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;JUSTASSI&quot;"/>
      <value value="&quot;JUSTASSI2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="swarm_size_check" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4801"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;REVERSE&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;JUSTASSI&quot;"/>
      <value value="&quot;JUSTASSI2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Test_timecheck" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4801"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;REVERSE&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;JUSTASSI&quot;"/>
      <value value="&quot;JUSTASSI2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="global_linear" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4801"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;REVERSE&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;JUSTASSI&quot;"/>
      <value value="&quot;JUSTASSI2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="global_linear_STOPCLUST" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4801"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.1"/>
      <value value="0.2"/>
      <value value="0.3"/>
      <value value="0.4"/>
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="R-ZERO_IG_global+local" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4801"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0015"/>
      <value value="0.004"/>
      <value value="0.0065"/>
      <value value="0.009"/>
      <value value="0.0115"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_90_60" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_80_70" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_75_70_backround35" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_73_71_backround20" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_90_60_NO_OBSTACLE" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_80_70_NO_OBSTACLE" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_120_60_20BACK_NO_OBSTACLE" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_60_30_10BACK_NO_OBSTACLE" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_60_30_10BACK_NO_OBSTACLE" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_130_100_30BACK_SWARM_SIZE" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_130_100_30BACK_SWARM_SIZE_DOUBLETIME" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="9604"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_130_100_60BACK_SWARM_SIZE" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_70_50_10BACK_SWARM_SIZE" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_120_60_BACK_SWARM_SIZE" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="All_global+localround_120_40_BACK_SWARM_SIZE" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;STOPCLUST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SOCDIST3_120_40_BACK_SWARM_SIZE" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SOCDIST3&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SOCDIST3_global+localround_130_100_30BACK_SWARM_SIZE" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="xcor_multi">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SOCDIST3&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="1"/>
      <value value="4"/>
      <value value="7"/>
      <value value="10"/>
      <value value="13"/>
      <value value="16"/>
      <value value="19"/>
      <value value="22"/>
      <value value="25"/>
      <value value="28"/>
      <value value="31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_ebugs">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Maze_Test" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;SOCDIST2&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="140VS80BR20_MAZE_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="140VS80BR20_NO_OBST_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="140VS80BR20_REAL_WALL_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="80VS40BR0_MAZE_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="80VS40BR0_REAL_WALL_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="80VS40BR0_NO_OBST_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="180VS130BR60_MAZE_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="180VS130BR60_REAL_WALL_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="180VS130BR60_NO_OBST_World1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>R1time1</metric>
    <metric>R1time2</metric>
    <metric>R1time3</metric>
    <metric>R1time4</metric>
    <metric>R1time5</metric>
    <metric>R1AVG</metric>
    <metric>R2time1</metric>
    <metric>R2time2</metric>
    <metric>R2time3</metric>
    <metric>R2time4</metric>
    <metric>R2time5</metric>
    <metric>R2AVG</metric>
    <metric>NETTO</metric>
    <metric>PEME</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;RANDOMWALK&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SOCDIST2&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;RANDOM&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global + local round + backround&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Obstacle_check" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="NoNoise_in_World" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Obstacle_check" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="5"/>
      <value value="10"/>
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Noise_in_World_10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;NO_OBSTACLE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="10"/>
      <value value="25"/>
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Noise25_different_obstacles_10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Noise0_different_obstacles_10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Noise40_different_obstacles_10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="MANY_different_obstacles_15" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
      <value value="&quot;BARRICADES&quot;"/>
      <value value="&quot;DOUBLEBLOCK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="MANY__obstacles_15_NOISE10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
      <value value="&quot;BARRICADES&quot;"/>
      <value value="&quot;DOUBLEBLOCK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="MANY_different_obstacles_10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
      <value value="&quot;BARRICADES&quot;"/>
      <value value="&quot;DOUBLEBLOCK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="MANY_different_obstacles_10_NOISE10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;R-ZERO&quot;"/>
      <value value="&quot;UPHILL&quot;"/>
      <value value="&quot;RANDOMWALK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
      <value value="&quot;BARRICADES&quot;"/>
      <value value="&quot;DOUBLEBLOCK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SLOWTEST_different_obstacles_10" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SLOWSOCDIST&quot;"/>
      <value value="&quot;SUPERSLOWSOCDIST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
      <value value="&quot;BARRICADES&quot;"/>
      <value value="&quot;DOUBLEBLOCK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="SLOWTEST_different_obstacles_10_SILENCE" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="4802"/>
    <metric>mean [xcor] of ebugs</metric>
    <enumeratedValueSet variable="METHOD">
      <value value="&quot;BEECLUST&quot;"/>
      <value value="&quot;SOCDIST&quot;"/>
      <value value="&quot;SLOWSOCDIST&quot;"/>
      <value value="&quot;SUPERSLOWSOCDIST&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="angle">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="OBSTACLE">
      <value value="&quot;MAZE&quot;"/>
      <value value="&quot;REAL_WALL&quot;"/>
      <value value="&quot;BOTTLENECK_A&quot;"/>
      <value value="&quot;BOTTLENECK_B&quot;"/>
      <value value="&quot;BARRICADES&quot;"/>
      <value value="&quot;DOUBLEBLOCK&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nr_turtles">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="JAS">
      <value value="0.52"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOI">
      <value value="0.9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOX">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="std_Q">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influence_gradient">
      <value value="0.0065"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOY">
      <value value="53"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="countdown">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="speed">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="EBUGDISTRI">
      <value value="&quot;AT-START&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius">
      <value value="14"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="simp_i">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LOS">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="RV">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gradient">
      <value value="&quot;global linear&quot;"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
